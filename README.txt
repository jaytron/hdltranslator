#License#

This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Copyright 2014.


# Instructions #

FILES:
vhdl_templates - source text directory for the translator to build up from.

Translator.py - Main class for translating files.

CodeWriter.py/Parser.py - Utility classes for Translator.py.


DIRECTIONS:
1. Unzip the Translator into the directory containing your .hdl file
	or a subfolder of several .hdl files.
2. Navigate to the directory containing the translator using the
	command line.
3. Enter the following command:
	python Translator.py <your file or directory name>
	
For example:
	"python Translator.py Foo.hdl"
	Would translate a single HDL file named Foo.hdl

	"python Translator.py Bar"
	Would look for a local directory named "Bar", translate
	any HDL files in the directory and output the VHDL files
	in "Bar".

NOTES:
* The Translator is not functional for sequential logic chips,
	only combinational.
* The Translator is not functional for HDL files with improper
	syntax. It must pass the HardwareSimulator before it can
	be translated.


Please send any feedback or bugs to litch2ja@cmich.edu