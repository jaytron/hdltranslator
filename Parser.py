##########################################################################################
# This file is part of HDLTranslator.
#
# HDLTranslator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HDLTranslator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HDLTranslator.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014.
##########################################################################################

import re

##########################################################################################
# Parser Module:
# Handles the parsing of a single .hdl file, and encapsulates access to the chip code.
# It reads HDL components, parses them, and provides convenient access to the pins/parts
# and part components.
# In addition, it removes all white space and comments.
##########################################################################################

class ParserAPI:
    ######################################################################################
    # Name: initializer
    # Description: Reads in the input file/stream, formats it, and segments the chip pieces
    #                in to logical containers.
    # Arguments: Input file/stream
    # Returns:
    ######################################################################################
    def __init__(self, fileName):
        self.fOpen = open(fileName)
        self.hdl_code = ''
        self.chip_name = ''
        self.input_pins = []
        self.output_pins = []
        self.local_pins = []
        self.not_pins = {}

        self.parts = []

        # Remove comments and blank spaces
        for line in self.fOpen.readlines():
            if '//' in line:
                line = line.partition('//')[0]
                line = line.strip()
                # if there is still code in front of a comment
                if line:
                    self.hdl_code += line
            # If the line doesn't have a comment, append the code
            elif line.strip():
                self.hdl_code += line.strip()

        # Remove multi-line comments using a regex
        self.hdl_code = re.sub(re.compile("/\*.*?\*/",re.DOTALL ) ,"" ,self.hdl_code)

        # Remove newline characters
        self.hdl_code.translate(None,'\t\n')

        self.fOpen.close()

    ######################################################################################
    # Name: chipName
    # Description: Searches the HDL file for the CHIP keyword and returns the chip name.
    # Arguments:
    # Returns:  self.chipName (string)
    ######################################################################################
    def chipName(self):
        # Only call this method if it hasn't been called already
        if not self.chip_name:
            # Grabs the string between CHIP and the initial curly bracket
            self.chip_name = self.hdl_code[self.hdl_code.find('CHIP ')+5: self.hdl_code.find('{')].strip()

        return self.chip_name

    ######################################################################################
    # Name: inputPins
    # Description: Searches the HDL file for the IN keyword and places the input pin names
    #                in a list.
    # Arguments:
    # Returns:  self.input_pins (string list)
    ######################################################################################
    def inputPins(self):
        pins = None
        # Only call the method if it hasn't been called before
        if not self.input_pins:
            # grabs the string between the IN keyword and the first semicolon
            pins = self.hdl_code[self.hdl_code.find('IN')+2: self.hdl_code.find(';')].strip().replace(" ","")
            # then breaks it into a list
            self.input_pins = pins.split(',')
            # fix pin names and busing syntax
            self.input_pins = self.cleanPins(self.input_pins)

        return self.input_pins

    ######################################################################################
    # Name: outputPins
    # Description: Searches the HDL file for the OUT keyword and places the output pin names
    #                in a list.
    # Arguments:
    # Returns:  self.output_pins (string list)
    ######################################################################################
    def outputPins(self):
        pins = None
        # Only call the method if it hasn't been called before
        if not self.output_pins:
            # Ignore the hdl code all the way up to the output pins
            pins = self.hdl_code[self.hdl_code.find('OUT')+3:]

            # then grab the pin names up to the first semicolon
            pins = pins[:pins.find(';')].strip().replace(" ","")
            self.output_pins = pins.split(',')
            self.output_pins = self.cleanPins(self.output_pins)

        return self.output_pins

    ######################################################################################
    # Name: localPins
    # Description: Searches the hdl file PARTS for pins *not* mapped to the chips IO.
    #                Ignores NOT chips.
    # Arguments:
    # Returns:  self.local_pins (string list)
    ######################################################################################
    def localPins(self):
        pins = []
        # Only call the method if it hasn't been called before and if there are parts in the chip.
        if not self.local_pins and self.getParts() > 0:
            # ignore duplicate local pin usage
            ignore = []

            # Iterate through the chips and look for pins used locally
            for chip in self.getParts():
                # if the local variable is an output for a "Not" chip, ignore it
                if chip['name'].lower() == 'not':
                    # essentially ignore the 2nd index of the 2nd tuple in the list i.e.
                    # the value of Not(out=) UNLESS it's piped to the bus of an OUT pin.
                    not_out_pin = chip['pins'][1][1]
                    # This checks the pin name ignoring any indexing. i.e. it grabs the
                    # pin name just prior to the brackets indicating the index.
                    # Example, if the pin was labeled "x[1]", then it would only grab "x" for
                    # the comparison.
                    # I LOVE, string slicing.
                    if not_out_pin[:not_out_pin.find('(')] != [x for x in self.rmBusing(self.outputPins(), translated=True)]:
                        ignore.append(not_out_pin)
                else:
                    # look through all of the pin names in the chip without considering sub buses.
                    for pin_name in self.rmBusing([x[1] for x in chip['pins']], translated=True):
                            # we haven't already captured it
                        if ((pin_name not in pins) and
                            # it's not a temp for 'Not'
                            (pin_name not in ignore) and
                            # and it's not defined in the IN/OUT pins
                            (pin_name not in self.rmBusing(self.outputPins(), translated=True)) and
                            (pin_name not in self.rmBusing(self.inputPins(),  translated=True))):
                            # Add it to the local pins. Whew!
                            # No wonder we only want to do that once per chip.
                            pins.append(pin_name)

            self.local_pins = pins

        return self.local_pins

    ######################################################################################
    # Name: hasLocalPins
    # Description: Convenience method to determine if local variables need to be
    #                instantiated in the VHDL architecture section.
    # Arguments:
    # Returns:  boolean
    ######################################################################################
    def hasLocalPins(self):
        if len(self.localPins()) > 0:
            return True
        else:
            return False
    ######################################################################################
    # Name: notPins
    # Description: Returns a dictionary object to quickly lookup and swap placeholder
    #                HDL Not chip pins with the VHDL keyword expression.
    # Arguments:
    # Returns:  A lookup dictionary of not pins
    # Example: An HDL Not chip that appeared as:
    #            Not(in=foo,out=notFoo);
    #          Would output into the dictionary as:
    #            {'notFoo':'( not foo )'}
    ######################################################################################
    def notPins(self):
        # Only call this method once per chip
        if not self.not_pins:
            for chip in self.getParts():
                if chip['name'].lower() == 'not':
                    # Capture the output pin name of the Not chip
                    placeHolder = chip['pins'][1][1]
                    if placeHolder not in self.not_pins.keys():
                        # Capture the input pin name of the Not chip
                        notting_pin = chip['pins'][0][1]
                        # Add the key, value pair to the dictionary
                        self.not_pins[placeHolder] = '( not %s )' % notting_pin.lstrip().strip()

        return self.not_pins


    ######################################################################################
    # Name: cleanPins
    # Description: Checks all pin groups for reserved VHDL keyword collisions and renames
    #                pins where appropriate. Also replaces brackets with parenthesis for busing.
    # Arguments:   A list of pin names to clean and format
    # Returns:    The formatted list
    ######################################################################################
    def cleanPins(self, pin_list):
        reserved = ['in','out','constant','signal','begin','process']

        cleaned_pins = []
        # A list of all pin names with no busing referenced
        pin_names = self.rmBusing(pin_list, translated=False)

        for index, pin in enumerate(pin_list):
            if pin_names[index] in reserved:
                # i.e. if it's a keyword with no array attached
                if pin_names[index] == pin:
                    cleaned_pins.append(pin+'_pin')
                else:
                    # There is busing used, so clean the pin name and re-attach the VHDL busing
                    new_pin = pin_names[index]+'_pin'+pin[pin.find('['):]
                    new_pin = new_pin.replace('[','(').replace(']',')')
                    cleaned_pins.append(new_pin)
            else:
                # the pin name is not a reserved keyword.
                if pin_names[index] != pin:
                    # There is busing used, so clean the pin name and re-attach the VHDL busing
                    new_pin = pin.replace('[','(').replace(']',')')
                    cleaned_pins.append(new_pin)
                else:
                    cleaned_pins.append(pin)
        return cleaned_pins


    ######################################################################################
    # Name: cleanSingle
    # Description: Checks a specified pin for VHDL keywords and returns a cleaned pin name.
    #                Also checks for busing and adjusts for VHDL syntax.
    # Arguments: A pin name
    # Returns:    A pin name that does not conflict with VHDL keyword conventions or busing.
    ######################################################################################
    def cleanSingle(self,pin_entry):
        reserved = ['in','out','constant','signal','begin','process']
        # since rmBusing returns a list, just grab the first index to get the string literal
        pin_name = self.rmBusing([pin_entry])[0]

        if pin_name in reserved:
            if pin_name == pin_entry:
                return pin_entry+'_pin'
            else:
                new_pin = pin_name+'_pin'+pin_entry[pin_entry.find('['):]
                new_pin = new_pin.replace('[','(').replace(']',')')
                return new_pin
        else:
            # There is sub-busing
            if pin_name != pin_entry:
                new_pin = pin_entry.replace('[','(').replace(']',')')
                return new_pin
            else:
                return pin_entry

    ######################################################################################
    # Name: getParts
    # Description: Searches the HDL file for the PARTS keyword and produces a list of
    #                part tuples that identify the part name and it's pin assignments.
    # Arguments:
    # Returns:  List of part dictionaries. each dict['name'] is the chip name and
    #            dict['pins'] provides the list of pin tuples.
    #
    # Example: If a part in the HDL code was written as:
    #            Add(a=foo,b=bar,out=bazOut);
    #          The getParts method would organize the part as:
    #            {'name':'Add', 'pins':[('a','foo'),('b','bar'),('out','bazOut')]}
    #              i.e. dict['name'] = 'Add'
    #              i.e. dict['pins'] = [('a','foo'),('b','bar'),('out','bazOut')]
    ######################################################################################
    def getParts(self):
        temp_parts = None
        # Again, only call the method if it hasn't been called before
        if not self.parts:
            # Ignore the hdl code all the way to the PARTS keyword and end at the last semicolon
            temp_parts = self.hdl_code[self.hdl_code.find('PARTS:')+6: self.hdl_code.rfind(';')].strip().lstrip()

            # if no parts were found, return nothing
            if len(temp_parts) == 0:
                return ''

            # ELSE Split the found parts into a list
            temp_parts = temp_parts.split(';')

            parts = []

            # Iterates through each part in the list
            for part in temp_parts:
                # captures the chip name
                chip_name = part[:part.find('(')].lstrip().strip()
                # captures a string of the chip pins (e.g. "(a=p1,b=p2,out=chipOut)" )
                pins = part[part.find('(')+1: part.rfind(')')].strip().lstrip()
                # split the pins apart
                pins = pins.split(',')

                part_dict = {}
                pin_tuples = []

                # iterate through each pin assignment in the chip
                for pin in pins:
                    mapping = pin.split('=')
                    # Create a tuple with the input pin of the subchip and output of the chip
                    currpin_map = self.cleanSingle(mapping[0].lstrip().strip()),self.cleanSingle(mapping[1].lstrip().strip())
                    # Place the mapping in a tuple and add it to the pin_tuples list
                    pin_tuples.append(currpin_map)

                part_dict['name'] = chip_name
                part_dict['pins'] = pin_tuples

                # Add a dictionary with the chip name and it's pin list (of tuples) to the part list
                parts.append(part_dict)

        # After all of the parts have been processed, return the parts
        return parts

    ######################################################################################
    # Name: rmBusing
    # Description: Convenience method to remove bus references on pin names, since it was
    #                becoming an issue.
    #
    # Arguments: A list of pin names with possible VHDL busing. Boolean indicating if the
    #            input list has already been translated. Defaults to False.
    # Returns:  A formatted list of pin names with any busing references removed.
    #
    # Example: If a list of pins includes "foo[4]", the returned list just contains "foo"
    ######################################################################################
    def rmBusing(self, pin_list, translated=False):
        ret_list = []
        if not translated:
            for entry in pin_list:
                if '[' in entry:
                    entry = entry[:entry.find('[')]
                    entry = entry.strip().lstrip()
                ret_list.append(entry)
        else:
            for entry in pin_list:
                if '(' in entry:
                    entry = entry[:entry.find('(')]
                    entry = entry.strip().lstrip()
                ret_list.append(entry)

        return ret_list

    ######################################################################################
    # Name: getBusedPins
    # Description: Convenience method to locate all pins with busing in a pin name list.
    # Arguments: A list of pin names.
    # Returns:  A sub list of pins that contain busing.
    ######################################################################################
    def getBusedPins(self, pin_list):
        ret_list = []
        for entry in pin_list:
            # if the entry has explicit busing add it to the list
            if '[' in entry:
                ret_list.append(entry)
            # else if the entry has implicit busing (checking against the input/output
            # bus pins.
            elif (entry in self.rmBusing([pin_name for pin_name in self.inputPins() if '[' in pin_name]) or
                  (entry in self.rmBusing([pin_name for pin_name in self.outputPins() if '[' in pin_name]))):
                ret_list.append(entry)

        return ret_list

    ######################################################################################
    # Name: hasArrayPins
    # Description: Convenience method to check if the chip uses array IO and busing.
    # Returns:  boolean.
    ######################################################################################
    def hasArrayPins(self):
        # If either of the input or output pins are bused, the chip has array pins.
        if (self.getBusedPins( self.inputPins() ) or self.getBusedPins( self.outputPins() )) > 0:
            return True
        else:
            return False
