##########################################################################################
# This file is part of HDLTranslator.
#
# HDLTranslator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HDLTranslator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HDLTranslator.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014.
##########################################################################################

import os
##########################################################################################
# CodeWriter Module:
# Translates HDL components into VHDL code.
##########################################################################################

class CodeWriterAPI:
    ######################################################################################
    # Name: initializer
    # Description: Opens the output file/stream and gets ready to write into it.
    # Arguments: output file/stream
    # Returns:
    ######################################################################################
    def __init__(self, fileStream):
        self.fWriter = open(fileStream, 'w')
        # global variable to increment for every external chip for unique naming
        self.externalChipCount = 0
        self.framework = ''

    ######################################################################################
    # Name: writeFramework
    # Description: Creates the framework for the generated VHDL chip.
    # Arguments: chip name (string), directory of framework template files
    # Returns:
    ######################################################################################
    def writeFramework(self, chip, framework_dir):

        template = open(os.path.join(framework_dir,'cmb_framework.vhd'))

        # Read the template into a string to edit the template tags
        for line in template.readlines():
            self.framework += line

        self.framework = self.framework.replace('<chip_name>',chip)
        self.framework = self.framework.replace('<arch_name>',chip+'_arch')
        template.close()

    ######################################################################################
    # Name: writePinIO
    # Description: translates HDL pin IO to VHDL pins and data types
    # Arguments: input_pins and output_pins
    # Returns:
    ######################################################################################
    def writePinIO(self, input_pins, output_pins):

        self.format_pins = ''
        # Helper function to remove redundant code
        def IOWriter(pin_list, IO):
            for pin in pin_list:
                pin_string = ''
                if '(' in pin:
                    # Breaks up an HDL array pin into a dict of components to display in VHDL.
                    pin_dict = self.pinArrayFormat(pin)
                    # Translates pin busing based on if an array or index is provided
                    upBound = pin_dict['upBound'] if pin_dict['upBound'] else int(pin_dict['size'])-1
                    lowBound = pin_dict['lowBound'] if pin_dict['lowBound'] else '0'
                    pin_string = (
                        '\t\t%s : %s std_logic_vector(%s downto %s);\n' %(pin_dict['name'], IO, upBound, lowBound)
                    )
                else:
                    pin_string = '\t\t%s : %s std_logic;\n'%(pin, IO)

                self.format_pins += pin_string

        IOWriter(input_pins, 'in')
        IOWriter(output_pins, 'out')

        # slice off the last semicolon and newline character
        self.format_pins = self.format_pins[:-2]

        # place the pin assignments in the VHDL framework
        self.framework = self.framework.replace('<chip_pins>',self.format_pins)

    ######################################################################################
    # Name: writeLocalPins
    # Description: translates HDL pin IO to VHDL pins and data types
    # Arguments: chip name (string), is_sequential, has_local_pins
    # Returns:
    ######################################################################################
    def writeLocalPins(self, local_pins, has_local):

        self.format_pins = ''
        # Helper function to remove redundant code (although now that it's been copy pasted...)
        def IOWriter(pin_list):
            for pin in pin_list:
                pin_string = ''
                if '(' in pin:
                    pin_dict = self.pinArrayFormat(pin)
                    upBound = pin_dict['upBound'] if pin_dict['upBound'] else int(pin_dict['size'])-1
                    lowBound = pin_dict['lowBound'] if pin_dict['lowBound'] else '0'
                    pin_string = (
                        '\t\tsignal %s : std_logic_vector(%s downto %s);\n' %(pin_dict['name'], upBound, lowBound)
                    )
                else:
                    pin_string = '\t\tsignal %s : std_logic;\n'%pin

                self.format_pins += pin_string

        if has_local:
            IOWriter(local_pins)

        self.framework = self.framework.replace('<local_pins>',self.format_pins)

    ######################################################################################
    # Name: pinArrayFormat
    # Description: helper function to interpret HDL pin arrays. Searches for pin name,
    #                array size, and/or start/end points
    # Arguments: pin_array (pin name followed by brackets. e.g. "in[6]" or foo[0..8]")
    # Returns: A dictionary for the pin name, array size, upper bound, and lower bound.
    ######################################################################################
    def pinArrayFormat(self, pin):
        pin_dict = {'name':'','upBound':'','lowBound':'','size':''}
        pin_dict['name'] = pin[:pin.find('(')].strip()
        # If an upper bound and lower bound are specified
        if '..' in pin:
            pin_dict['lowBound'] = pin[pin.find('(')+1:pin.find('..')].lstrip().strip()
            pin_dict['upBound'] = pin[pin.find('..')+2:pin.rfind(')')].lstrip().strip()
        else:
            pin_dict['size'] = pin[pin.find('(')+1:pin.rfind(')')].lstrip().strip()

        return pin_dict

    ######################################################################################
    # Name: writeLogic
    # Description: Iterates through the chips and translates them to VHDL components,
    #                then places them in the framework.
    # Arguments: A list of parts from the parser and the not pins
    # Returns:
    ######################################################################################
    def writeLogic(self, parts, not_pins):
        format_logic = ''
        for part in parts:
            if part['name'].lower != 'not': # ignoring all not chips
                if part['name'].lower() == 'and' or part['name'].lower() == 'or':
                    format_logic += self.writeAndOr(part, not_pins)
                elif part['name'].lower() == 'nand' or part['name'].lower() == 'nor':
                    format_logic += self.writeNandNor(part, not_pins)
                else:
                    # prepend any port mappings to the chip logic.
                    format_logic = self.writeExternalChip(part, not_pins) + format_logic

        self.framework = self.framework.replace('<chip_logic>', format_logic)

    ######################################################################################
    # Name: writeAndOr
    # Description: Translates the And/Or chips from HDL into their keywords in VHDL
    # Arguments: two pin names or expressions to "and" or "or"
    # Returns: output <= (exp1 and|or exp2)
    ######################################################################################
    def writeAndOr(self, part, not_pins):
        eval_a = part['pins'][0][1]
        eval_b = part['pins'][1][1]
        output = part['pins'][2][1]

        if eval_a in not_pins.keys():
            eval_a = not_pins[eval_a]

        if eval_b in not_pins.keys():
            eval_b = not_pins[eval_b]

        if part['name'].lower() == 'and':
            comparitor = 'and'
        else:
            comparitor = 'or'

        return '\t\t%s <= (%s %s %s);\n' %(output, eval_a, comparitor, eval_b)

    ######################################################################################
    # Name: writeNandNor
    # Description: Translates the Nand/Nor chips from HDL into their keywords in VHDL
    # Arguments: two pin names or expressions to "nand" or "nor"
    # Returns: output <= (exp1 nand|nor exp2)
    ######################################################################################
    def writeNandNor(self, part, not_pins):
        eval_a = part['pins'][0][1]
        eval_b = part['pins'][1][1]
        output = part['pins'][2][1]

        if eval_a in not_pins.keys():
            eval_a = not_pins[eval_a]

        if eval_b in not_pins.keys():
            eval_b = not_pins[eval_b]

        if part['name'].lower() == 'nand':
            comparitor = 'nand'
        else:
            comparitor = 'nor'

        return '\t\t%s <= (%s %s %s);\n' %(output, eval_a, comparitor, eval_b)

    ######################################################################################
    # Name: writeExternalTrip
    # Description: Translate any user created chips into VHDL port mappings
    # Arguments: User created pins
    # Returns: a port mapping for an external chip
    ######################################################################################
    def writeExternalChip(self, part, not_pins):
        chip_name = part['name']
        pins = part['pins']
        # set up chip header
        mapping = '\t%s_unit%s: entity work.%s(%s_arch)\n\t\tport map(' % (chip_name, self.externalChipCount, chip_name, chip_name)
        # increment the chip count for the next iteration
        self.externalChipCount += 1
        # set up pin mappings
        for pin in pins:
            mapping += '%s=>%s, ' % (pin[0], pin[1])

        mapping = mapping[:-2]
        mapping += ');\n'

        return mapping
    ######################################################################################
    # Name: close
    # Description: Closes the output file.
    # Arguments:
    # Returns:
    ######################################################################################
    def close(self):
        self.fWriter.write(self.framework)
        self.fWriter.close()
