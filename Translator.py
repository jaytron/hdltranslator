##########################################################################################
# This file is part of HDLTranslator.
#
# HDLTranslator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HDLTranslator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HDLTranslator.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014.
##########################################################################################

import sys, os
import Parser
import CodeWriter

# Grab the current directory to access the frameworks:
framework_dir = os.path.join(os.path.abspath(os.curdir),'vhdl_templates')

# Requires the .hdl filename/directory argument on the command line
if len(sys.argv) == 2:
    arg = sys.argv[1]
else:
    print "Incorrect arguments. Please pass a single file or directory. "
    sys.exit()

# Checks if the argument is a file or directory and sets up the output file.
if os.path.isfile(arg):
    fOutName = arg.replace('.hdl', '.vhd')
    hdlFiles = [arg]
else:
    hdlDir = os.path.split(arg)
    # if a directory was provided, changes to the current directory to output in.
    os.chdir(hdlDir[1])
    # parses out all of the hdl files.
    hdlFiles = [f for f in os.listdir('.') if f[-4:] == '.hdl']


print 'Located .hdl file(s).  Will output the VHDL file(s) in the given directory. '

for chip in hdlFiles:
    # Parse the HDL file into logical components
    hdl_chip = Parser.ParserAPI(chip)

    # Open the output stream to the VHDL file
    _writer = CodeWriter.CodeWriterAPI(hdl_chip.chipName()+'.vhd')

    print "Chip name: ", hdl_chip.chipName()
    print "Input pins: ", hdl_chip.inputPins()
    print "output pins: ", hdl_chip.outputPins()
    print "local pins: ", hdl_chip.localPins()

    # Write the framework for the VHDL file
    _writer.writeFramework(hdl_chip.chipName(), framework_dir)

    # Place the pin IO in the framework
    _writer.writePinIO(hdl_chip.inputPins(), hdl_chip.outputPins())
    _writer.writeLocalPins(hdl_chip.localPins(), hdl_chip.hasLocalPins())

    # Process the parts and translate the Not keywords.
    _writer.writeLogic(hdl_chip.getParts(), hdl_chip.notPins())

    # Close the VHDL file and finalizes
    _writer.close()
