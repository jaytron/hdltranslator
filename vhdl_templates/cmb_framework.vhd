library ieee;
use ieee.std_logic_1164.all;

--A generic entity template to place the I/O pins
--of the HDL chip

entity <chip_name> is
	port (
	
<chip_pins>
	
	); 

end <chip_name>;

architecture <arch_name> of <chip_name> is
<local_pins>

	begin
		
<chip_logic>

end <arch_name>;